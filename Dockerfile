from php:8.0

RUN apt-get update -qq && apt-get install -yqq \
    git \
    libzip-dev \
    zip \
    libpng-dev \
    libxml2-dev \
  &&  docker-php-ext-install -j$(nproc) pdo_mysql zip gd dom xml
    
RUN pecl install apcu-5.1.21 \
  && docker-php-ext-enable apcu --ini-name 10-docker-php-ext-apcu.ini

RUN curl -sS https://getcomposer.org/installer \
  | php -- --install-dir=/usr/local/bin --filename=composer
